#ifndef EDITOR_H
#define EDITOR_H

#include <QPlainTextEdit>
#include <QPlainTextDocumentLayout>
#include <QTextBlock>
#include <QSize>
#include <QDebug>

class QPaintEvent;
class QMouseEvent;

class EditorLayout : public QPlainTextDocumentLayout
{
    Q_OBJECT

public:
    EditorLayout(QTextDocument *document) : QPlainTextDocumentLayout(document) {
    }

    void emitDocumentSizeChanged() {
        emit documentSizeChanged(documentSize());
    }
};

class Editor : public QPlainTextEdit
{
    Q_OBJECT

public:
    Editor();

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

private slots:
    void updateCursorPosition();

private:
    QTextBlock foldedBlockAt(const QPoint &point);

    bool folded;
};

#endif
