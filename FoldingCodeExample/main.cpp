
#include <QtGui>
#include "editor.h"

int main(int argc, char **args)
{
    QApplication app(argc, args);

    Editor editor;
    editor.show();

    return app.exec();
}
