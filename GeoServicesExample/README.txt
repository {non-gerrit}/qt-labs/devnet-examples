Different Views of the World Examples
=====================================

The C++ versions of these examples can be built using the qmake project
files supplied. The plugins built by the osmplugin and mapimageplugin
project files should be installed in the plugins/geoservices directory
in your Qt installation. The project files provide installation rules
for this that can be invoked with "make install" or similar.

The mapimageplugin example requires a map.jpg file to be installed in
the mapimageplugin/files directory before it can be built.

The Python version of the services example has been tested with PyQt 4.8
and PyQtMobility 1.0.

About Qt Quarterly
==================

Qt Quarterly is a newsletter available to Qt developers. Every quarter we
aim to publish an issue that we hope will bring added insight and pleasure
to your Qt programming, with high-quality technical articles written by Qt
experts.

See http://doc.qt.nokia.com/qq for more information.

License Information
===================

The examples contained in this package is provided under the following
license:

   Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
   All rights reserved.
   Contact: Nokia Corporation (qt-info@nokia.com)

   This file is part of the documentation of Qt. It was originally
   published as part of Qt Quarterly.

   $QT_BEGIN_LICENSE:LGPL$
   Commercial Usage
   Licensees holding valid Qt Commercial licenses may use this file in
   accordance with the Qt Commercial License Agreement provided with the
   Software or, alternatively, in accordance with the terms contained in
   a written agreement between you and Nokia.

   GNU Lesser General Public License Usage
   Alternatively, this file may be used under the terms of the GNU Lesser
   General Public License version 2.1 as published by the Free Software
   Foundation and appearing in the file LICENSE.LGPL included in the
   packaging of this file.  Please review the following information to
   ensure the GNU Lesser General Public License version 2.1 requirements
   will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.

   In addition, as a special exception, Nokia gives you certain additional
   rights.  These rights are described in the Nokia Qt LGPL Exception
   version 1.1, included in the file LGPL_EXCEPTION.txt in this package.

   GNU General Public License Usage
   Alternatively, this file may be used under the terms of the GNU
   General Public License version 3.0 as published by the Free Software
   Foundation and appearing in the file LICENSE.GPL included in the
   packaging of this file.  Please review the following information to
   ensure the GNU General Public License version 3.0 requirements will be
   met: http://www.gnu.org/copyleft/gpl.html.

   If you have questions regarding the use of this file, please contact
   Nokia at qt-info@nokia.com.
   $QT_END_LICENSE$
