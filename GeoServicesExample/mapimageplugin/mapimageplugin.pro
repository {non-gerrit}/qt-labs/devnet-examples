TEMPLATE      = lib
CONFIG       += plugin
PLUGIN_TYPE   = geoservices
TARGET        = mapimagegeoservices

QT           += network

CONFIG       += mobility
MOBILITY      = location

HEADERS       = imggeomappingmanagerengine.h \
                imggeomapreply.h \
                imggeoserviceproviderfactory.h \
                imggeotiledmapdata.h \
                imgrunnable.h

SOURCES       = imggeomappingmanagerengine.cpp \
                imggeomapreply.cpp \
                imggeoserviceproviderfactory.cpp \
                imggeotiledmapdata.cpp \
                imgrunnable.cpp

RESOURCES     = mapimageplugin.qrc

target.path = $$[QT_INSTALL_PLUGINS]/$$PLUGIN_TYPE

INSTALLS += target
