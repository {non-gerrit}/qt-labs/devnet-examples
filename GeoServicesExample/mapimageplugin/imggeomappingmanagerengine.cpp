/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the documentation of Qt. It was originally
** published as part of Qt Quarterly.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include "imggeomappingmanagerengine.h"
#include "imggeotiledmapdata.h"

MapImageGeoMappingManagerEngine::MapImageGeoMappingManagerEngine(
    const QMap<QString, QVariant> &parameters,
    QGeoServiceProvider::Error *error, QString *errorString)
: QGeoTiledMappingManagerEngine(parameters)
{
    Q_UNUSED(error)
    Q_UNUSED(errorString)
    Q_UNUSED(parameters)

    setTileSize(QSize(256, 256));
    setMinimumZoomLevel(0.0);
    setMaximumZoomLevel(4.0);

    QList<QGraphicsGeoMap::MapType> types;
    types << QGraphicsGeoMap::SatelliteMapDay;
    setSupportedMapTypes(types);

    QList<QGraphicsGeoMap::ConnectivityMode> modes;
    modes << QGraphicsGeoMap::OfflineMode;
    setSupportedConnectivityModes(modes);

    sourceImage.load(":files/map.jpg");
}

MapImageGeoMappingManagerEngine::~MapImageGeoMappingManagerEngine()
{
}

QGeoMapData *MapImageGeoMappingManagerEngine::createMapData()
{
    QGeoMapData *data = new MapImageGeoTiledMapData(this);
    if (!data)
        return 0;

    data->setConnectivityMode(QGraphicsGeoMap::OnlineMode);
    return data;
}

QGeoTiledMapReply *MapImageGeoMappingManagerEngine::getTileImage(const QGeoTiledMapRequest &request)
{
    // Obtain the relevant part of the source image.
    MapImageGeoMapReply *mapReply = new MapImageGeoMapReply(request, sourceImage);

    return mapReply;
}
