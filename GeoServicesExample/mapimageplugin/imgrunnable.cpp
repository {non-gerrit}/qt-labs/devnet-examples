/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the documentation of Qt. It was originally
** published as part of Qt Quarterly.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QBuffer>
#include "imgrunnable.h"

MapImageRunnable::MapImageRunnable(const QImage &sourceImage, int zoom, int row,
                                   int column)
    : m_sourceImage(sourceImage), m_zoom(zoom), m_row(row), m_column(column)
{
}

void MapImageRunnable::run()
{
    // The number of tiles along each axis is 2^zoom.
    // zoom is a value from 0 to 4, so the number of tiles are 1 to 16.
    int number = 1 << m_zoom;

    // Divide the source image into 2^zoom by 2^zoom tiles.
    int tileWidth = m_sourceImage.size().width() / number;
    int tileHeight = m_sourceImage.size().height() / number;

    // Calculate the coordinates for the top-left corner of the tile.
    int tx = m_column * tileWidth;
    int ty = m_row * tileHeight;

    QImage tileImage = m_sourceImage.copy(tx, ty, tileWidth, tileHeight);
    QImage scaledImage = tileImage.scaled(256, 256);

    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QBuffer::WriteOnly);
    scaledImage.save(&buffer, "PNG");
    buffer.close();

    emit finished(data);
}
