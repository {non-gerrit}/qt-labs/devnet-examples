/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the documentation of Qt. It was originally
** published as part of Qt Quarterly.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include <QGeoCoordinate>
#include <QGeoMappingManagerEngine>
#include <QGraphicsGeoMap>
#include "mapwindow.h"

using namespace QtMobility;

MapWindow::MapWindow()
{
    scene = new QGraphicsScene(this);

    findServices();

    QGraphicsView *view = new QGraphicsView(this);
    view->setScene(scene);
    setCentralWidget(view);
}

void MapWindow::findServices()
{
    QMenu *serviceMenu = menuBar()->addMenu(tr("&Services"));

    actionGroup = new QActionGroup(this);
    actionGroup->setExclusive(true);
    connect(actionGroup, SIGNAL(triggered(QAction *)),
            this, SLOT(selectService(QAction *)));

    foreach (const QString &name, QGeoServiceProvider::availableServiceProviders()) {

        QGeoServiceProvider *service = new QGeoServiceProvider(name);
        services[name] = service;
        QAction *action = serviceMenu->addAction(name);
        action->setCheckable(true);
        actionGroup->addAction(action);
    }

    if (services.isEmpty()) {

        QGraphicsTextItem *item = scene->addText(
            tr("Failed to find any map services. Please ensure that "
               "the location services plugins for Qt Mobility have "
               "been built and, if necessary, set the QT_PLUGIN_PATH "
               "environment variable to the location of the Qt Mobility "
               "plugins directory."));
        item->setTextWidth(300);
        QAction *action = serviceMenu->addAction(tr("No services"));
        action->setEnabled(false);
    } else
        actionGroup->actions()[0]->trigger();
}

void MapWindow::selectService(QAction *action)
{
    action->setChecked(true);
    QString name = action->text();

    scene->clear();

    QGeoServiceProvider *service = services[name];
    if (service->error() != QGeoServiceProvider::NoError) {

        QGraphicsTextItem *item = scene->addText(
            tr("The \"%1\" service failed with the following error:\n'"
               "%2").arg(name).arg(service->errorString()));
        item->setTextWidth(300);
    } else {
        QGeoMappingManager *manager = service->mappingManager();
        QGraphicsGeoMap *geoMap = new QGraphicsGeoMap(manager);
        scene->addItem(geoMap);

        geoMap->resize(300, 300);
        geoMap->setCenter(QGeoCoordinate(37.76, -25.675));
        geoMap->setMapType(QGraphicsGeoMap::TerrainMap);
        geoMap->setZoomLevel(12);
    }
}
