#!/usr/bin/env python

#############################################################################
##
## Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
## Contact: Nokia Corporation (qt-info@nokia.com)
##
## This file is part of the documentation of Qt. It was originally
## published as part of Qt Quarterly.
##
## $QT_BEGIN_LICENSE:LGPL$
## Commercial Usage
## Licensees holding valid Qt Commercial licenses may use this file in
## accordance with the Qt Commercial License Agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Nokia.
##
## GNU Lesser General Public License Usage
## Alternatively, this file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
##
## In addition, as a special exception, Nokia gives you certain additional
## rights.  These rights are described in the Nokia Qt LGPL Exception
## version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## If you have questions regarding the use of this file, please contact
## Nokia at qt-info@nokia.com.
## $QT_END_LICENSE$
##
#############################################################################

import sys

from PyQt4.QtGui import QActionGroup, QApplication, QGraphicsScene, QGraphicsView, QMainWindow
from PyQt4.QtMobility.QtLocation import *

class MapWindow(QMainWindow):

    def __init__(self):

        QMainWindow.__init__(self)

        self.scene = QGraphicsScene()

        self.findServices()

        view = QGraphicsView()
        view.setScene(self.scene)
        self.setCentralWidget(view)

    def findServices(self):

        serviceMenu = self.menuBar().addMenu(self.tr("&Services"))

        self.services = {}
        self.actionGroup = QActionGroup(self)
        self.actionGroup.setExclusive(True)
        self.actionGroup.triggered.connect(self.selectService)

        for name in QGeoServiceProvider.availableServiceProviders():

            service = QGeoServiceProvider(name)
            self.services[name] = service
            action = serviceMenu.addAction(name)
            action.setCheckable(True)
            self.actionGroup.addAction(action)

        if not self.services:

            item = self.scene.addText(
                self.tr("Failed to find any map services. Please ensure that "
                        "the location services plugins for Qt Mobility have "
                        "been built and, if necessary, set the QT_PLUGIN_PATH "
                        "environment variable to the location of the Qt Mobility "
                        "plugins directory."))
            item.setTextWidth(300)
            action = serviceMenu.addAction(self.tr("No services"))
            action.setEnabled(False)
        else:
            self.actionGroup.actions()[0].trigger()

    def selectService(self, action):

        action.setChecked(True)
        name = action.text()

        self.scene.clear()

        service = self.services[name]
        if service.error() != service.NoError:

            item = self.scene.addText(
                self.tr('The "%1" service failed with the following error:\n'
                        "%2").arg(name).arg(service.errorString()))
            item.setTextWidth(300)

        else:
            self.manager = service.mappingManager()
            self.geoMap = QGraphicsGeoMap(self.manager)
            self.scene.addItem(self.geoMap)

            self.geoMap.resize(300, 300)
            self.geoMap.setCenter(QGeoCoordinate(37.76, -25.675))
            self.geoMap.setMapType(self.geoMap.TerrainMap)
            self.geoMap.setZoomLevel(12)


if __name__ == "__main__":

    app = QApplication(sys.argv)
    window = MapWindow()
    window.show()
    sys.exit(app.exec_())
