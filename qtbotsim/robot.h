/*
 * In the original BSD license, both occurrences of the phrase "COPYRIGHT
 * HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
 *
 * Here is the license template:
 *
 * Copyright (c) 2011, Johan Thelin <johan@thelins.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * Neither the name of Johan Thelin nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <QObject>

#include <QPixmap>
#include <QPainter>

class Robot : public QObject
{
	Q_OBJECT

	Q_PROPERTY(bool penActive READ penActive WRITE setPenActive NOTIFY penActiveChanged)
	Q_PROPERTY(bool brightForward READ brightForward NOTIFY brightForwardChanged)
	Q_PROPERTY(qreal leftEngineSpeed READ leftEngineSpeed WRITE setLeftEngineSpeed NOTIFY leftEngineSpeedChanged)
	Q_PROPERTY(qreal rightEngineSpeed READ rightEngineSpeed WRITE setRightEngineSpeed NOTIFY rightEngineSpeedChanged)

public:
	explicit Robot(QObject *parent = 0);

	void step(QPixmap *pixmap);
	void drawOverlay(QPainter &painter);

	bool brightForward() const;
	bool penActive() const;
	qreal leftEngineSpeed() const;
	qreal rightEngineSpeed() const;

signals:
	void brightForwardChanged(bool);
	void penActiveChanged(bool);

	void leftEngineSpeedChanged(qreal);
	void rightEngineSpeedChanged(qreal);

public slots:
	void reset(const QPixmap &pixmap);

	void setPenActive(bool);

	void setLeftEngineSpeed(qreal);
	void setRightEngineSpeed(qreal);

private:
	void setPosition(const QPointF &p);
	QPointF position() const;

	void setDirection(qreal d);
	qreal direction() const;

	void setBrightForward(bool);

	void updateSensors(const QPixmap &);
	QPointF movementChange() const;
	qreal directionChange() const;

	bool m_brightForward;
	bool m_penActive;
	qreal m_leftEngineSpeed;
	qreal m_rightEngineSpeed;

	QPointF m_position;
	qreal m_direction;
};

#endif // ROBOT_H
