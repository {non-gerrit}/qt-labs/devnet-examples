#-------------------------------------------------
#
# Project created by QtCreator 2011-11-13T15:52:13
#
#-------------------------------------------------

QT       += core gui declarative

TARGET = qtbotsim
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    robotwidget.cpp \
    robot.cpp

HEADERS  += widget.h \
    robotwidget.h \
    robot.h

FORMS    += widget.ui
