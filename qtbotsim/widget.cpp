/*
 * In the original BSD license, both occurrences of the phrase "COPYRIGHT
 * HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
 *
 * Here is the license template:
 *
 * Copyright (c) 2011, Johan Thelin <johan@thelins.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * Neither the name of Johan Thelin nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "widget.h"
#include "ui_widget.h"

#include <QTimer>

#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QDeclarativeComponent>

#include "robot.h"

Widget::Widget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Widget)
{
	m_timer = new QTimer(this);
	m_timer->setInterval(50);

	connect(m_timer, SIGNAL(timeout()), this, SLOT(on_stepButton_clicked()));

	ui->setupUi(this);
	m_robot = new Robot(this);

	ui->widget->setRobot(m_robot);

	QDeclarativeEngine *engine = new QDeclarativeEngine(this);
	engine->rootContext()->setContextProperty("robot", m_robot);
	QDeclarativeComponent component(engine);
	component.setData("import QtQuick 1.0\n"
			  "StateGroup {\n"
			  "    states: [State { name: \"turn\"\n"
			  "    PropertyChanges { target: robot; leftEngineSpeed: -0.5; rightEngineSpeed: 1.0; } },\n"
			  "    State { name: \"forward\"\n"
			  "    PropertyChanges { target: robot; leftEngineSpeed: 0.5; rightEngineSpeed: 0.5; } } ]\n"
			  "    state: robot.brightForward?\"forward\":\"turn\"\n"
			  "}\n"
			  , QUrl());
	QObject *controller = component.create();

	foreach(QDeclarativeError e, component.errors())
		qDebug("ERROR: %d %s", e.line(), qPrintable(e.description()));

	controller->setParent(this);
}

Widget::~Widget()
{
	delete ui;
}

void Widget::on_clearButton_clicked()
{
	m_timer->stop();
	ui->widget->reset();
}

void Widget::on_stepButton_clicked()
{
	ui->widget->step();
}

void Widget::on_runButton_clicked()
{
	if (m_timer->isActive())
		m_timer->stop();
	else
		m_timer->start();
}
