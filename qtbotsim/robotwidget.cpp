/*
 * In the original BSD license, both occurrences of the phrase "COPYRIGHT
 * HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
 *
 * Here is the license template:
 *
 * Copyright (c) 2011, Johan Thelin <johan@thelins.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * Neither the name of Johan Thelin nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "robotwidget.h"

#include <QPainter>

#include "robot.h"

RobotWidget::RobotWidget(QWidget *parent)
	: QWidget(parent)
	, m_robot(0)
{
	m_paper = QPixmap(500, 500);
	reset();

	setMinimumSize(sizeHint());
	setMaximumSize(sizeHint());
}

void RobotWidget::setRobot(Robot *robot)
{
	m_robot = robot;
	reset();
}

QSize RobotWidget::sizeHint()
{
	return m_paper.size();
}

void RobotWidget::reset()
{
	m_paper.fill(Qt::white);
	QPainter p(&m_paper);
	QPen pen(Qt::black);
	pen.setWidth(10);
	p.setPen(pen);
	p.drawEllipse(100, 100, 300, 300);
	if (m_robot)
		m_robot->reset(m_paper);
	update();
}

void RobotWidget::step()
{
	m_robot->step(&m_paper);
	update();
}

void RobotWidget::paintEvent(QPaintEvent *)
{
	QPainter p(this);
	p.drawPixmap(0, 0, m_paper);
	p.setRenderHint(QPainter::Antialiasing);
	if (m_robot)
		m_robot->drawOverlay(p);
}
