/** This file is part of _PACKAGE_NAME_**

Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).*
All rights reserved.

Contact:  Nokia Corporation (INSERT EMAIL ADDRESS)

You may use this file under the terms of the BSD license as follows:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
* Neither the name of Nokia Corporation and its Subsidiary(-ies) nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <QtGui>
#include <QtDeclarative>

class Object : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor myColor READ getMyColor NOTIFY myColorChanged)

public:
    Object()
    {
        hue = 0;
        startTimer(100);
    }

    QColor getMyColor()
    {
        return QColor::fromHslF(hue, 1, 0.5);
    }

    void timerEvent(QTimerEvent *)
    {
        hue += 0.01;
        if (hue > 1)
            hue = 0;
        emit myColorChanged();
    }

signals:
    void myColorChanged();

private:
    qreal hue;
 };

#include "main.moc"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    Object myObj;
    QDeclarativeView view;
    view.rootContext()->setContextProperty("rootItem", (QObject *)&myObj);
    view.setSource(QUrl::fromLocalFile("main.qml"));
    view.show();
    return app.exec();
}